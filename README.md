Filip's boilerplate 
--------------
Modified HTML5 responsive boilerplate. 
Responsive part is easy to disable, just remove susy and the susy imports.

VERSION 1.1
--------------
Changes:

 - midway.js add to vendor js folder. Less then 1 kb center responsive div lib.
 - folder structure static / www
 - compass config.rb file added
 - compass config.rb includes requires susy grid
 - sass folder added and .scss files follow _partals folder structure
 - head.load.js added for smarter js loading
 - sublime project files created
 - sync_to_www.sh with the rsync function. Clean tansfer to the www folder - usage,in terminal open project folder and launch the sync_to_www.sh
 - default media query's placed in css
 - kaleistyleguide included for automatic documentation of css
#